﻿using UnityEngine;

namespace EasySurvivalScripts
{
    public class PlayerCamera : MonoBehaviour
    {

        [Header("Input Settings")]
        public string MouseXInput;
        public string MouseYInput;

        [Header("Common Camera Settings")]
        public float mouseSensitivity;

        [Header("FPS Camera Settings")]
        public Vector3 FPS_CameraOffset;
        public Vector2 FPS_MinMaxAngles;

        Transform FPSController;
        float xClamp;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            xClamp = 0;
            FPSController = GetComponentInParent<PlayerMovement>().transform;
        }

        // Update is called once per frame
        void Update()
        {

            RotateCamera();

        }

        void RotateCamera()
        {
            float mouseX = Input.GetAxis(MouseXInput) *
                (mouseSensitivity * Time.deltaTime);
            float mouseY = Input.GetAxis(MouseYInput) *
                (mouseSensitivity * Time.deltaTime);
            Vector3 eulerRotation = transform.eulerAngles;

            xClamp += mouseY;

            xClamp = Mathf.Clamp(xClamp, FPS_MinMaxAngles.x,
                FPS_MinMaxAngles.y);

            eulerRotation.x = -xClamp;
            transform.eulerAngles = eulerRotation;
            FPSController.Rotate(Vector3.up * mouseX);
        }
    }
}