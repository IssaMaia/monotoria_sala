﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasySurvivalScripts
{
    public enum PlayerStates
    {
        Idle,
        Walking,
    }

    public class PlayerMovement : MonoBehaviour
    {
        public PlayerStates playerStates;

        [Header("Inputs")]
        public string HorizontalInput = "Horizontal";
        public string VerticalInput = "Vertical";
        public string RunInput = "Run";

        [Header("Player Motor")]
        public static float walkSpeed = 2f;
        [Range(1f,15f)]
        public Transform FootLocation;

        CharacterController characterController;

        // Use this for initialization
        void Start()
        {
            characterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            //handle controller
            HandlePlayerControls();
        }

        void HandlePlayerControls()
        {
            float hInput = Input.GetAxisRaw(HorizontalInput);
            float vInput = Input.GetAxisRaw(VerticalInput);

            Vector3 fwdMovement = characterController.isGrounded == true ? transform.forward * vInput : Vector3.zero;
            Vector3 rightMovement = characterController.isGrounded == true ? transform.right * hInput : Vector3.zero;

            characterController.SimpleMove(Vector3.ClampMagnitude(fwdMovement + rightMovement, 1f) * walkSpeed);

            //Managing Player States
            if (characterController.isGrounded)
            {
                if (hInput == 0 && vInput == 0)
                    playerStates = PlayerStates.Idle;
                else
                {
                    playerStates = PlayerStates.Walking;
                }
            }
        }
    }
}