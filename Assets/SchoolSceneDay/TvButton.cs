﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TvButton : MonoBehaviour {

	public int buttonID;
	private TV_manager tvManager;

	void Start()
	{

		tvManager = GetComponentInParent<TV_manager> ();
	}

	public void ProcessClick()
	{
		tvManager.ProcessClick (buttonID);
	}
}
