﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addball : MonoBehaviour {
	int count = 0;
	int totalnumber = 0;
	// Use this for initialization
	void Start () {
		totalnumber = gameObject.transform.childCount;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.K)) {
			if (count < totalnumber && count >= 0) {
				gameObject.transform.GetChild (count).gameObject.SetActive (true);
			}
			count++;
		}

		if (Input.GetKeyDown (KeyCode.J)) {
			count--;
			if (count < totalnumber && count >= 0) {
				gameObject.transform.GetChild (count).gameObject.SetActive (false);
			}
		}

		if (Input.GetKeyDown (KeyCode.L)) {
			for(int i = 0; i < totalnumber; i++) {
				gameObject.transform.GetChild (i).gameObject.SetActive (false);
			}
			count = 0;
		}


	}
}
