﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getTagInfo : MonoBehaviour {
	private RaycastHit hit;
	public GameObject letter;
	private int success = 0;
	private int miss = 0;
	private int count = 1;
	string filename = "/Users/ChenLIANG/Unity/test2/data.csv";
	int objectnum = 5;
	string[] task = {"Q", "M", "C", "G", "L"};
	bool getback = true;
	Vector3 letterPosition = Vector3.zero;
	//Quaternion letterRotation = Quaternion.identity;

	private float moveSpeed = 500f;
	private Rigidbody rbody;

	private Text _label;

	void Awake() {
		_label = GameObject.FindGameObjectWithTag("label").GetComponent<Text>();
	}

	void Start () {
		//System.IO.File.WriteAllText(filename,string.Empty);
		rbody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		RaycastHit hitinfo = new RaycastHit ();
		bool hit = Physics.Raycast (gameObject.transform.position, Camera.main.transform.forward, out hitinfo);

		if (Input.GetKeyDown (KeyCode.K) && count <= objectnum && getback) {
			PrintResult (hit, hitinfo);
			getback = false;
			rbody.useGravity = false;
			rbody.freezeRotation = true;
			rbody.velocity = Camera.main.transform.forward * moveSpeed * Time.deltaTime;

		}

		if (Input.GetKeyDown (KeyCode.L)) {

			_label.text = "";

			if (count < objectnum) {
				switchLetter (count);
			} else {
				Destroy (letter);
			}

			Vector3 center = new Vector3 (Screen.width / 2, Screen.height / 2, 1f);
			//letter.transform.position = Camera.main.ScreenToWorldPoint (center);
			transform.position = Camera.main.ScreenToWorldPoint (center);
			rbody.velocity = Vector3.zero;
			rbody.useGravity = false;
			getback = true;

			//rbody.rotation = Quaternion.identity;
		}

	}

	void PrintResult(bool hit, RaycastHit hitinfo){
		if (hit && hitinfo.collider.tag == letter.tag) {
			success++;
			Debug.Log ("success");
			_label.text = "ACERTOU";
		}else {
			miss++;
			Debug.Log ("miss");
			_label.text = "FALHOU";
		}
			
		count++;
	}

	void switchLetter(int count){
		letterPosition = letter.transform.position;
		GameObject newLetter = Instantiate (letter, letterPosition, letter.transform.rotation) as GameObject;
		newLetter.transform.parent = gameObject.transform;
		Destroy (letter);
		letter = newLetter;

		letter.GetComponent<TextMesh> ().text = task[count];
		letter.transform.localScale = new Vector3 (0.5f, 0.5f, 0.02f);
		letter.transform.tag = task[count];
	}

	void OnCollisionEnter(Collision collision)
	{
		rbody.useGravity = true;
		rbody.freezeRotation = true;
	}
	//wait for a path
	/*
	void LateUpdate(){
		if (Input.GetKeyDown (KeyCode.Q)) {
			System.IO.File.WriteAllText(filename,string.Empty);
			System.IO.StreamWriter csv = new System.IO.StreamWriter (filename, true);
			csv.WriteLine ("Total number of success: " + success);
			csv.WriteLine ("Total number of miss: "+ miss);
			Debug.Log("Recorded");
			csv.Close ();
		}
	}
	*/
}
