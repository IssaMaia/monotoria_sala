﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {
	private float moveSpeed = 1000f;
	Vector3 originPosition = new Vector3(0f, 1f, 2.6f);
	private Rigidbody rbody;
	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody> ();
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.K)) {
			rbody.useGravity = false;
			//rbody.AddForce(0f, 0f, -moveSpeed * 100);
			rbody.velocity = new Vector3(0f, 0f, -moveSpeed * Time.deltaTime);
			transform.position = originPosition;
		}
	}

	void OnCollisionEnter (Collision collision)
	{
		rbody.useGravity = true;
		//transform.position = originPosition;
	}

}
