﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		InvokeRepeating("SpawnObject", 2, 1);
		CancelInvoke ();
	}

	void SpawnObject()
	{
		Instantiate(gameObject, new Vector3(0, 2, 0), Quaternion.identity);
	}
}
