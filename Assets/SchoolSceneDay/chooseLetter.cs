﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class chooseLetter : MonoBehaviour {
	Color black;
	Color Ginger;
	Color yello;

	bool ifRestart = false;
	int letterCount = 0;
	int numGame = 0;
	int numLetter = 5;
	string[,] letterList = new string[2, 5]{{"A", "U", "H", "O", "B"},
											{"G", "C", "T", "R", "N"}};
	int ListNum = 0;
	
	string[] rightAnswerFour = new string[6]{"B", "O", "A", "A", "O", "U"};
	string[] rightAnswerFive = new string[6]{"N", "R", "C", "T", "R", "C"};

	GameObject AnswerFOUR;
	GameObject AnswerFIVE;
	GameObject Answer;
	bool needChange = false;
	public bool right = false;

	public Text Print;
	int score = 0;

	bool isFour = true;
	int fourCount = 0;
	int fiveCount = 0;
	int Count = 0;

	int target = 0;

	float contadorDeTempo = 0;

	//{"CIN  MA", "C  TTLE", "DOCT  R", "H  RSE", "CH  IR"};
	// Use this for initialization
	void Start () {
		
		AnswerFOUR = GameObject.Find ("AnswerFOUR");
		AnswerFIVE = GameObject.Find ("AnswerFIVE");
		Answer = AnswerFOUR;
		ListNum = 0;
		contadorDeTempo = Time.timeSinceLevelLoad;


		for (int i = 0; i < numLetter; i++)
			transform.GetChild (i).GetComponent<TextMesh> ().text = letterList [ListNum, i];
		Print.text = "Score/ Game Played: " + score.ToString() + "/ " + numGame.ToString();

		black = transform.GetChild (0).GetComponent<TextMesh> ().color;
		Ginger = transform.GetChild (5).GetComponent<TextMesh> ().color;
		GameObject word = GameObject.Find ("Word");
		yello = word.GetComponent<TextMesh> ().color;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown (KeyCode.J)) {
			if (isFour) {
				target = fiveCount;
			} else {
				target = fourCount;
			}
			if (target <= 5) {
				needChange = true;
			}
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			transform.GetChild (changeNum(letterCount - 1)).GetComponent<TextMesh> ().color = black;
			transform.GetChild (changeNum(letterCount)).GetComponent<TextMesh> ().color = Ginger;
			letterCount++;
		}

        /*if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.GetChild(changeNum(letterCount)).GetComponent<TextMesh>().color = black;
            transform.GetChild(changeNum(letterCount - 1)).GetComponent<TextMesh>().color = Ginger;
            letterCount--;
            Debug.Log(letterCount);
        }*/

        if (Input.GetKeyDown (KeyCode.Return) && Count < 7 && numGame < 12) {
			
			GameObject child = Answer.transform.GetChild (Count).gameObject;
			if (!ifRestart && Count < 6) {
				string letterChosed = letterList [ListNum, changeNum (letterCount - 1)];
				child.SetActive (true);

				child.GetComponent<TextMesh> ().text = letterChosed;
				right = (letterChosed == rightletter ());
				if (right) {
					///verifica se esta certo
					child.GetComponent<TextMesh> ().color = yello;
					score++;
					//conta o tempo do jogo
					float tempoQueLevou = Time.timeSinceLevelLoad - contadorDeTempo;
					WriteTxt.WriteNewLine (WriteTxt.desktopFolder, "Tempo", "Tempo da jodada = " + tempoQueLevou, false);
					contadorDeTempo = Time.timeSinceLevelLoad;
				} else {
					child.GetComponent<TextMesh> ().color = Color.red;
				}
			} else if (ifRestart) {
				Count = addGameNum (); //add one to fourCount or FiveCount depending on isFour
				if(Count == 6){
					needChange = true;
				}
				if (needChange && target <= 5) {
					needChange = false;
					Answer = switchAnswer ();//switch isFour, Count, ListNum
				}
				for (int i = 0; i < numLetter; i++) {
					transform.GetChild (i).GetComponent<TextMesh> ().color = black;
					transform.GetChild (i).GetComponent<TextMesh> ().text = letterList [ListNum, i];
				}
				child.SetActive (false);
				numGame++;
				Print.text = "Score/ Game Played: " + score.ToString() + "/ " + numGame.ToString();
				Debug.Log (numGame);
				if (numGame >= 12) {
					Print.text = "Score/ Game Played: " + score.ToString() + "/ " + numGame.ToString() + ". all finished";
				}
			}
			ifRestart = !ifRestart;
			letterCount = 0;
		}

        if(numGame == 12)
            SceneManager.LoadScene(1);
    }

	int changeNum(int letterCount){
		if (letterCount > numLetter - 1) {
			return letterCount % numLetter;
		} else if (letterCount < 0) {
			return letterCount + numLetter;
		}
		return letterCount;
	}


	int addGameNum(){
		if (isFour) {
			fourCount++;
			return fourCount;
		} else {
			fiveCount++;
			return fiveCount;
		}
	}

	int getGameNum(){
		if (isFour) {
			return fourCount;
		} else {
			return fiveCount;
		}
	}

	string rightletter (){
		string answer;
		if (isFour) {
			answer = rightAnswerFour [fourCount];
		} else {
			answer = rightAnswerFive [fiveCount];
		}
		Debug.Log ("Right Answer: " + answer);
		Debug.Log ("Four Count: " + fourCount);
		Debug.Log ("Five Count: " + fiveCount);
		return answer;
	}

	GameObject switchAnswer(){
		GameObject target;
		if (isFour) {
			ListNum = 1;
			Count = fiveCount;
			target = AnswerFIVE;
		} else {
			ListNum = 0;
			Count = fourCount;
			target = AnswerFOUR;
		}
		Debug.Log ("Switch to: " + ListNum.ToString ());
		isFour = !isFour;
		return target;

	}


}
