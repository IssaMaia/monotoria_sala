﻿using UnityEngine;
//using System.Collections;
using UnityEngine.Video;
using UnityEngine.UI;
public class TV_manager : MonoBehaviour
{
	private VideoPlayer videoPlayer;
	[SerializeField]
	private VideoClip[] clips;
	private int currentChannel = 0;
	private RawImage _videoTexture;
    void Start()
    {
		videoPlayer = GetComponent<VideoPlayer> ();
		_videoTexture = GetComponentInChildren<RawImage> ();
		_videoTexture.enabled = false;
		videoPlayer.clip = clips [0];
		videoPlayer.SetTargetAudioSource (0, GetComponent<AudioSource> ());
    }
		

	public void ProcessClick(int id)
	{
		switch(id)
		{
		case 1:
			_videoTexture.enabled = true;
			videoPlayer.Play ();
				break;
		case 2:
			if (currentChannel > 0) {
				currentChannel--;
				videoPlayer.clip = clips [currentChannel];
				videoPlayer.Play ();
			}
				break;
		case 3:
			if (currentChannel < clips.Length - 1) {
				currentChannel++;
				videoPlayer.clip = clips[currentChannel];
				videoPlayer.Play ();
			}

				break;
		case 4:
			videoPlayer.Pause ();
			_videoTexture.enabled = false;
				break;
		}

	}
}