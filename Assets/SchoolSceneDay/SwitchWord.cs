﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchWord : MonoBehaviour {
	int gameCount = 0;
	public Text Print;

	const int numGame = 6;
	int fourCount = 0;
	int fiveCount = 0;
	int Count = 0;

	string[] wordListFour = new string[6]{"     O L A", "V A S    ", "G     T O", "M E S    ", "R     S A", "S     M O"};
	string[] wordListFive = new string[6]{"B A     H O", "C A     R O", "D I S     O", "F R U     A", "L I V     O", "S A     O"};
	public string[] wordList;
	bool ifRestart = false;
	bool ifchange = false;
	int target = 0;

	GameObject UnderlineFour;
	GameObject UnderlineFive;
	GameObject Underline;

	bool end = false;

	// Use this for initialization
	void Start () {
		wordList = wordListFour;
		GetComponent<TextMesh> ().text = wordList [0];

		UnderlineFour = GameObject.Find ("UnderlineFour");
		UnderlineFive = GameObject.Find ("UnderlineFive");
		Underline = UnderlineFour;
	}
	
	// Update is called once per frame
	void Update () {
		end = (fourCount == 5 && fiveCount == 5);

		if (Input.GetKeyDown (KeyCode.J)) {
			string trail;
			//int target = 0;
			if (wordList == wordListFour) {
				trail = "five";
				target = fiveCount;

			} else {
				trail = "four";
				target = fourCount;
			}
			if (target <= 5) {
				ifchange = true;
				Print.text = "Switched to Trial " + trail;
			} else {
				Print.text = "target trail is finished";
			}
		}
			

		if (Input.GetKeyDown (KeyCode.Return) && gameCount < 12 && Count < 6) {
			
			if (ifRestart) {
                ((SopaLevelInstance)LevelInstance.Singleton).EnableWords(true);
                Count = addCount (); //add one to fourCount or FiveCount depending on isFour
				if(Count == 6){
					ifchange = true;
					string trail;
					if (wordList == wordListFour) {
						trail = "five";
					} else {
						trail = "four";
					}
					Print.text = "Switched to Trial " + trail;
				}
				if (ifchange && target <= 5) {
					ifchange = false;
					Count = swichList ();//switch isFour, Count, ListNum
				}
				Debug.Log("Switch Word Count: " + Count.ToString());

				if (!(fiveCount > 5 && fourCount > 5)) {
					GetComponent<TextMesh> ().text = wordList [Count];
					//Debug.Log ("set true: " + num.ToString());
					Underline.transform.GetChild (Count).gameObject.SetActive (true);
				}
				if (Input.GetKeyUp(KeyCode.Escape))
				{
					Application.LoadLevel ("SchoolSceneDay");
				}

				/*
				gameCount++;
				//Debug.Log("Switch Word Count: " + Count.ToString());
				if (Count == 5) {
					ifchange = true;
				}

				if (ifchange) {
					if (wordList == wordListFour) {
						fourCount++;
					}else{
						fiveCount++;
					}
					num = swichList ();
					ifchange = false;
				} else {
					num = addCount ();
				}


				Debug.Log ("Word List Num " + num.ToString());
				if (!end & num < 6) {
					GetComponent<TextMesh> ().text = wordList [num];
					//Debug.Log ("set true: " + num.ToString());
					Underline.transform.GetChild (num).gameObject.SetActive (true);
				}
				*/

			} else {
				//Debug.Log ("set false: " + num.ToString());
				Underline.transform.GetChild (Count).gameObject.SetActive (false);
                ((SopaLevelInstance)LevelInstance.Singleton).EnableWords(false);

			}
			ifRestart = !ifRestart;

		}



	}

	int addCount(){
		if (wordList == wordListFour) {
			fourCount++;
			Count = fourCount;
		}else{
			fiveCount++;
			Count = fiveCount;
		}
		if (Count > 5)
			end = true;
		return Count;
	}

	int swichList(){
		if (wordList == wordListFour) {
			wordList = wordListFive;
			Underline = UnderlineFive;
			return fiveCount;
		}else{
			wordList = wordListFour;
			Underline = UnderlineFour;
			return fourCount;
		}
	}
}
