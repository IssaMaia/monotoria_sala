﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
    public float floor, pos;
    public GameObject VRPlayer;
    public GameObject Portas;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        pos = transform.position.y;
        
        if(pos>=0.5 && pos<=19.5 && floor == 12)
        {
            transform.Translate(0, 0.05f, 0); 
        }
        else if(pos >= 19.5 && floor == 12)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }

        if (pos >= 0.5 && pos <= 38.2 && floor == 13)
        {
            transform.Translate(0, 0.05f, 0);
        }
        else if (pos >= 38.2 && floor == 13)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }

        if (pos >= 19.5 && pos <= 38.2 && floor == 23)
        {
            transform.Translate(0, 0.05f, 0);
        }
        else if (pos >= 38.2 && floor == 23)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }
        //------------------------------------------------------------------ Descer
        if (pos >= 0.6 && floor == -21)
        {
            transform.Translate(0, -0.05f, 0);
        }
        else if (pos <= 0.6 && floor == -21)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }

        if (pos >= 19.5 &&  floor == -32)
        {
            transform.Translate(0, -0.05f, 0);
        }
        else if (pos <= 19.5 && floor == -32)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }

        if (pos >=0.6 &&  floor == -31)
        {
            transform.Translate(0, -0.05f, 0);
        }
        else if (pos <= 0.6 && floor == -31)
        {
            Portas.GetComponent<Slide>().enabled = true;
            Portas.GetComponent<Slide>().statusDoor = 1;
            VRPlayer.GetComponent<RayCast>().doorStatus = 1;
        }


    }
}