﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pontos : MonoBehaviour {

	public static int count = 0;
	public GameObject pontos;
	public bool showPontos = false;
	public Text result;

	void Start(){
		count = 0;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			count++;
			if (showPontos) 
			{
				pontos.SetActive (true);
				result.text += count;
			}
		}

		Debug.Log (count);
	}
}
