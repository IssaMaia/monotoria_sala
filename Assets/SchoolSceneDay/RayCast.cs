﻿using UnityEngine;
using System.Collections;

public class RayCast : MonoBehaviour
{

    public GameObject elevatorDoor; // porta 
    public GameObject ElevatorStructure;
    public GameObject FirstPersonController;
    public GameObject Primeiro;
    public GameObject Segundo;
    public GameObject Terceiro;
    public int doorStatus; // 0 = fechada ; 1 = aberta
    public int i;
    public int andar;
    

   
    void cursorLock()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Screen.lockCursor = false;
        }
        else {
            Screen.lockCursor = true;
        }
    } // prende cursor ao meio do ecra e esconde-o. Ao primir ESC o cursor liberta e fica a mostra
    void Start()
    {
        andar = 1;
    }
    void Update()
    {
        cursorLock(); // prende o cursor do windows á crosshair
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // criamos o raycast
        RaycastHit hit = new RaycastHit(); // definimos a variavel do raycast
        bool ray = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit);

        if (Input.GetKey(KeyCode.J)) // ao ser carregado o botão da esquerda do rato 
        {
            
            // botao da parte de fora para chamar elevador
            if (hit.transform.gameObject.tag == "openDoorOutside" && doorStatus == 0)// ao carregar no objecto com a TAG, entra no if
            {
                
                elevatorDoor.GetComponent<Slide>().enabled = true;
                elevatorDoor.GetComponent<Slide>().statusDoor = 1;
                doorStatus = 1;
                
            }
            // INSIDE ELEVATOR
            if (hit.transform.gameObject.tag == "closeInside" && doorStatus == 1)// ao carregar no objecto com a TAG, entra no if
            {
                ElevatorStructure.GetComponent<Move>().enabled = false;
                elevatorDoor.GetComponent<Slide>().enabled = false;
                Debug.Log("Fecha");
                elevatorDoor.GetComponent<Slide>().statusDoor = 0;
                elevatorDoor.GetComponent<Slide>().enabled = true;
                doorStatus = 0;

                

            }

            if (hit.transform.gameObject.tag == "openInside" && doorStatus == 0)// ao carregar no objecto com a TAG, entra no if
            {

                elevatorDoor.GetComponent<Slide>().enabled = true;
                elevatorDoor.GetComponent<Slide>().statusDoor = 1;
                doorStatus = 1;

            }

           // Primeiro Andar
            if (hit.transform.gameObject.tag == "segundo" && doorStatus == 0 && andar == 1)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = 12;
                Primeiro.SetActive(false);
                Segundo.SetActive(true);
                andar = 2;
            }

            if (hit.transform.gameObject.tag == "terceiro" && doorStatus == 0 && andar == 1)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = 13;
                Primeiro.SetActive(false);
                Terceiro.SetActive(true);
                andar = 3;
            }
            // Segundo Andar
            if (hit.transform.gameObject.tag == "terceiro" && doorStatus == 0 && andar == 2)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = 23;
                Segundo.SetActive(false);
                Terceiro.SetActive(true);
                andar = 3;
            }

            if (hit.transform.gameObject.tag == "primeiro" && doorStatus == 0 && andar == 2)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = -21;
                Segundo.SetActive(false);
                Primeiro.SetActive(true);
                andar = 1;
            }
            // Terceiro Andar
            if (hit.transform.gameObject.tag == "segundo" && doorStatus == 0 && andar == 3)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = -32;
                Terceiro.SetActive(false);
                Segundo.SetActive(true);
                andar = 2;
            }

            if (hit.transform.gameObject.tag == "primeiro" && doorStatus == 0 && andar == 3)// ao carregar no objecto com a TAG, entra no if
            {
                Debug.Log("Ola");
                ElevatorStructure.GetComponent<Move>().enabled = true;
                ElevatorStructure.GetComponent<Move>().floor = -31;
                Terceiro.SetActive(false);
                Primeiro.SetActive(true);
                andar = 1;
            }


        }   
  }
}



