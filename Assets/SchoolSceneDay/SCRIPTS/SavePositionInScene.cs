﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SavePositionInScene : MonoBehaviour {

	public int defaultScene = 0;
	private static Vector3 pos = Vector3.zero;
	private static Vector3 rot = Vector3.zero;


	// Use this for initialization
	void Start () {
		if (pos == Vector3.zero && rot == Vector3.zero) 
		{
			return;
		}

		transform.position = pos;
		transform.eulerAngles = rot;
	}

	void Update()
	{
		if (SceneManager.GetActiveScene ().buildIndex == defaultScene) 
		{
			pos = transform.position;
			rot = transform.eulerAngles;
		}
	}
}
