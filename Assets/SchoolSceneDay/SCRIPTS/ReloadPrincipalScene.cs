﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadPrincipalScene : MonoBehaviour {

	[SerializeField] private int defaulSceneIndex;
	[SerializeField] public KeyCode reloadButtom = KeyCode.Escape;

	void Update () {
		if (Input.GetKeyDown (reloadButtom)) 
		{
			SceneManager.LoadScene (defaulSceneIndex);
		}
			
	}
}
