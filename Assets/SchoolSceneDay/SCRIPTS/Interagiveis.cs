﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interagiveis : MonoBehaviour
{

	public abstract void Interagir ();
	public abstract void PararDeInteragir ();

}
