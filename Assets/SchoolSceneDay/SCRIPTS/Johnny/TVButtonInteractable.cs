﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TVButtonInteractable : Interactable
{
    public enum ENUM_TVButton
    {
        OnOff = 0,
        Previous = 1,
        Next = 2
    }

    public ENUM_TVButton id = ENUM_TVButton.OnOff;
    public TV tv;

    private void Start()
    {
        switch (id)
        {
            case ENUM_TVButton.OnOff:
                interactMessage = "Ligar TV";
                tv.OnTvToggleOnOff += OnTVToggledOnOff;
                break;
            case ENUM_TVButton.Previous:
                interactMessage = "Canal Anterior";
                break;
            case ENUM_TVButton.Next:
                interactMessage = "canal Seguinte";
                break;
        }
    }

    public override void OnStartInteract()
    {
        base.OnStartInteract();

        switch (id)
        {
            case ENUM_TVButton.OnOff:
                tv.ToggleOnOff();
                break;
            case ENUM_TVButton.Previous:
                tv.PreviousChannel();
                break;
            case ENUM_TVButton.Next:
                tv.NextChannel();
                break;
        }
    }

    private void OnTVToggledOnOff(TV sender)
    {
        if (tv.IsOn)
            interactMessage = "Desligar TV";
        else
            interactMessage = "Ligar TV";
    }
}
