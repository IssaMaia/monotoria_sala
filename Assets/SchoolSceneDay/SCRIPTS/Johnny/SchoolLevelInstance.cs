﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SchoolLevelInstance : LevelInstance
{
    Scene objectFix_scene;
    float alpha = 0f;
    Texture2D texture;
    PavilhaoInteractable[] interactables;
    public AmpController ampController;

    protected override void Awake()
    {
        base.Awake();

        texture = new Texture2D(2, 2);
        for (int y = 0; y < 2; y++)
            for (int x = 0; x < 2; x++)
                texture.SetPixel(x, y, new Color(1, 1, 1, 1));
    }

    protected override void Start()
    {
        base.Start();

        interactables = GameObject.FindObjectsOfType<PavilhaoInteractable>();
    }

    void AddScene(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Additive);
        objectFix_scene = SceneManager.GetSceneByName(scene);
    }

    public void StartObjectFix(string scene)
    {

        GymDoorController[] gymDoors = GameObject.FindObjectsOfType<GymDoorController>();
        foreach (GymDoorController gymDoor in gymDoors)
            gymDoor.ShutDoor();

        AddScene(scene);

        foreach (PavilhaoInteractable interactable in interactables)
            interactable.gameObject.SetActive(false);
    }

    public void StopObjectFix()
    {
        if (!objectFix_scene.IsValid())
            return;

        Flash();

        GymDoorController[] gymDoors = GameObject.FindObjectsOfType<GymDoorController>();
        foreach (GymDoorController gymDoor in gymDoors)
            gymDoor.OpenDoor();

        foreach (PavilhaoInteractable interactable in interactables)
            interactable.gameObject.SetActive(true);

        SceneManager.UnloadSceneAsync(objectFix_scene);
    }

    private void OnGUI()
    {
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
    }

    private void Update()
    {
        if (alpha > 0)
            alpha -= 1f * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape))
            StopObjectFix();
    }

    void Flash()
    {
        alpha = 1.3f;
    }
}
