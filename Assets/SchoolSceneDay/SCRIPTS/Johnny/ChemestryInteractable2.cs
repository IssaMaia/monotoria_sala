﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChemestryInteractable2 : MonoBehaviour
{
    public static List<string> keys = new List<string>();

    public string Key = "None";

    public Material[] materials;
    public Material[] initMaterials;
    private float timer = 0;
    bool closed = false;

    public void StartGame()
    {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        renderer.materials = initMaterials;
        closed = false;
        Hide();
    }

    private void Update ()
    {
        timer = Mathf.Max(timer - Time.deltaTime, 0);
        if (timer == 0f)
            Hide();
    }

    public void Show()
    {
        timer = 0.6f;
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        if (closed)
            return;

        gameObject.SetActive(false);
    }

    public void Place()
    {
        closed = true;

        MeshRenderer renderer = GetComponent<MeshRenderer>();
        renderer.materials = materials;

        Show();
    }
}
