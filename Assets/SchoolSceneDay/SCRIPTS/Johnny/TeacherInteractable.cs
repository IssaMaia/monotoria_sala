﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherInteractable : Interactable
{
    public GameObject[] entities;
    public GymDoorController controller;

    public override void OnStartInteract()
    {
        base.OnStartInteract();

        if (entities == null)
            return;

        GameInstance.HUD.EnableChemestryMenu(true);

        active = false;
        show = false;
    }

    public void EnableGame()
    {
        foreach(GameObject entity in entities)
            entity.SetActive(false);

        controller.OpenDoor();

        active = true;
        show = true;
    }
}
