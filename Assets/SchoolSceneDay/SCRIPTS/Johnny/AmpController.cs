﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class AmpController : PlayerController, IPossessable
{
    private enum Side : byte
    {
        Left = 0,
        Right = 1,
    }

    public Camera cam;
    private bool started = false;
    private bool _waitingForInput = false;
    public GameObject bar;
    public Animation animation;
    private Coroutine slashCoroutine;
    public Renderer renderer;
    public Material[] materials;
    public AnimationClip[] clips;
    public Collider[] buttons;
    private int counter;
    private StreamWriter stream;
    private Side    textureSide,
                    rightSide;
    public int difficulty = 0;

    void Awake()
    {

    }

    private void StartGame()
    {
        started = true;
        counter = 0;
        bar.transform.rotation = Quaternion.Euler(0f, -48.095f, 0f);
        _waitingForInput = false;
        slashCoroutine = null;
        //GameInstance.GameState.EnableCursor(true, CursorLockMode.None);
        //GameInstance.HUD.EnableInteractMessage(null);
        stream = null;
        if (FileWriter.FileExists(GameInstance.HUD.studentName.text + "\\Amp.txt"))
            stream = FileWriter.OpenFile(GameInstance.HUD.studentName.text + "\\Amp.txt");
        else
            stream = FileWriter.WriteNewFile(GameInstance.HUD.studentName.text + "\\Amp.txt");

        FileWriter.WriteData(stream, "\tNova Experiência : " + DateTime.UtcNow.ToString());
    }

    private void StopGame()
    {
        started = false;

        Character character = GameObject.FindObjectOfType<Character>();
        if (character != null)
            GameInstance.PlayerControllers[0].Possess(character);
    }

    protected override void OnPossessed(PlayerController playerController)
    {
        base.OnPossessed(playerController);

        GameInstance.GameState.EnableCursor(true, CursorLockMode.None);
        cam.gameObject.SetActive(true);
        StartGame();
    }

    protected override void OnUnpossess(PlayerController playerController)
    {
        base.OnUnpossess(playerController);

        GameInstance.GameState.EnableCursor(false, CursorLockMode.Locked);
        cam.gameObject.SetActive(false);
        gameObject.SetActive(false);
        FileWriter.CloseFile(stream);
        stream = null;
    }

    protected override void Update()
    {
        base.Update();

        if (!started)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StopGame();
            return;
        }

        GameInstance.HUD.EnableInteractMessage(null);

        if (waitingForInput)
            return;

        if (slashCoroutine != null)
            return;

        slashCoroutine = StartCoroutine(Slash());
    }

    public void CheckLeft()
    {
        if (!_waitingForInput)
            return;

        if (rightSide == Side.Right)
        {
            Debug.Log("ERRADO");
            FileWriter.WriteData(stream, "\t\tEscolha errada.");
        }
        else
        {
            Debug.Log("CERTO");
            FileWriter.WriteData(stream, "\t\tEscolha certa.");
        }
        

        if (counter > 10)
            StopGame();
        _waitingForInput = false;
    }

    public void CheckRight()
    {
        if (!_waitingForInput)
            return;

        if (rightSide == Side.Left)
        {
            Debug.Log("ERRADO");
            FileWriter.WriteData(stream, "\t\tEscolha errada.");
        }
        else
        {
            Debug.Log("CERTO");
            FileWriter.WriteData(stream, "\t\tEscolha certa.");
        }

        if (counter > 10)
            StopGame();
        _waitingForInput = false;
    }

    private IEnumerator Slash()
    {
        counter++;
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.6f, 2f));

        if (UnityEngine.Random.Range(1, 100) < 50)
            SwitchTextures();

        yield return MoveBar(UnityEngine.Random.Range(0, 100) < 50);
        slashCoroutine = null;
    }

    private IEnumerator MoveBar(bool left)
    {
        waitingForInput = true;

        if (left)
        {
            animation.clip = clips[0];
            if (textureSide == Side.Right)
                rightSide = Side.Right;
                
        }
        else
        {
            animation.clip = clips[1];
            if (textureSide == Side.Right)
                rightSide = Side.Left;
        }

        animation["SlideLeft"].speed = difficulty == 0 ? 1f : 3f;
        animation["SlideRight"].speed = difficulty == 0 ? 1f : 3f;

        animation.Play();

        while (animation.isPlaying)
            yield return null;
    }

    private void SwitchTextures()
    {
        textureSide = (Side)UnityEngine.Random.Range(0, 2);
        renderer.material = materials[(int)textureSide];
    }

    private bool waitingForInput
    {
        get { return _waitingForInput; }

        set
        {
            _waitingForInput = value;

            buttons[0].enabled = value;
            buttons[1].enabled = value;
        }
    }
    public static Character Singleton { get; private set; }
}