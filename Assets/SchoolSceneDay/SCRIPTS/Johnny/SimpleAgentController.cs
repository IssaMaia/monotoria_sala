﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(UnityEngine.CharacterController))]
[RequireComponent(typeof(Animator))]
public class SimpleAgentController : Actor
{
    public Transform path;
    private Waypoint waypoint;
    NavMeshAgent navMeshAgent;
    private Animator animator;

    protected override void Start()
    {
        base.Start();

        navMeshAgent                    = GetComponent<NavMeshAgent>();
        navMeshAgent.avoidancePriority  = Random.Range(1, 2500);
        animator                        = GetComponent<Animator>();
        

        waypoint = GetNearestWaypoint();

        if (HasPath)
            navMeshAgent.SetDestination(GetValidPointOnNavMesh(waypoint.transform.position));
    }

    protected override void Update()
    {
        base.Update();

        animator.SetFloat("Speed", navMeshAgent.velocity.normalized.sqrMagnitude);

        if (!HasPath)
            return;

        if (!ReachedDestination && Vector3.Distance(transform.position, waypoint.transform.position) < navMeshAgent.stoppingDistance + 1)
        {
            ReachedDestination = true;
            StartCoroutine(TakeAction());
        }
    }

    private Waypoint GetNearestWaypoint()
    {
        if (path != null)
        {
            Transform child = null;
            Waypoint nearest = path.GetComponentInChildren<Waypoint>();
            for(int i = 0; i < path.childCount; i++)
            {
                child = path.GetChild(i);

                if (Vector3.Distance(transform.position, child.position) < Vector3.Distance(transform.position, nearest.transform.position))
                    nearest = child.GetComponent<Waypoint>();
            }

            return nearest;
        }

        return null;
    }

    private IEnumerator TakeAction()
    {
        if (waypoint != null)
            if (waypoint.action == Waypoint.ENUM_WaypointAction.Stop)
            {
                navMeshAgent.isStopped = true;
                yield return new WaitForSeconds(Random.Range(waypoint.timerMinMax.x, waypoint.timerMinMax.y));
            }

        int a = Random.Range(0, waypoint.links.Count - 1);
        waypoint = waypoint.links[a];

        if (waypoint != null)
        {
            if (HasPath)
            {
                ReachedDestination = false;
                navMeshAgent.isStopped = false;
                navMeshAgent.SetDestination(GetValidPointOnNavMesh(waypoint.transform.position));
            }
        }
    }

    private Vector3 GetValidPointOnNavMesh(Vector3 position)
    {
        NavMesh.SamplePosition(position, out NavMeshHit hit, 2f, NavMesh.AllAreas);
        return hit.position;
    }

    private bool HasPath { get { return waypoint != null; } }
    private bool ReachedDestination { get; set; }
}