﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ItemInteractable : Interactable
{
    public enum ItemType
    {
        Ball,
        Bottle,
        Book,
    }

    public enum ENUM_Color : ushort
    {
        black = 0,
        Red = 1,
        blue = 2,
        orange = 3,
        green = 4,
        silver = 5,
        yellow = 6,
    }

    Transform       _parent;
    Rigidbody       rb;
    float           distance = 0.8f;
    public ItemType type = ItemType.Ball;
    public ENUM_Color color = ENUM_Color.black;
    float           timer;
    bool            picked = false;

    protected override void Awake()
    {
        base.Awake();

        rb = GetComponent<Rigidbody>();
    }

    public override void OnStartInteract()
    {
        base.OnStartInteract();

        _parent = Camera.main.transform;
        if (rb != null)
            rb.useGravity = false;

        picked = true;
        transform.position = _parent.transform.position + _parent.transform.forward.normalized * distance;
    }

    protected override void Update()
    {
        base.Update();

        if (picked)
            timer += Time.deltaTime;
    }

    protected virtual void FixedUpdate()
    {
        if (_parent != null)
        {
            rb.velocity = ((_parent.transform.position + _parent.transform.forward.normalized * distance) - transform.position) * 500f * Time.fixedDeltaTime;

            if (Vector3.Distance(transform.position, _parent.transform.position + _parent.transform.forward.normalized * distance) > 0.45f)
                OnStopInteract(true);
        }
    }

    public override void OnStopInteract(bool over)
    {
        base.OnStopInteract(over);

        _parent = null;

        if (rb != null)
            rb.useGravity = true;

        picked = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CardBox")
        {
            string label = "";
            CardBox box = other.GetComponentInParent<CardBox>();
            if (box != null)
            {
                label = "Errada";
                if (box.sceneType == CardBox.ENUM_SceneType.Format)
                {
                    if (box.itemType == type)
                        label = "Correta";
                }
                else if (box.sceneType == CardBox.ENUM_SceneType.Color)
                {
                    if (box.color == color)
                        label = "Correta";
                }
                else if (box.sceneType == CardBox.ENUM_SceneType.ColorAndFormat)
                {
                    if (box.color == color && box.itemType == type)
                        label = "Correta";
                }
                

                StreamWriter writer = FileWriter.OpenFile(GameInstance.HUD.studentName.text + "\\Pavilhao.txt");
                FileWriter.WriteData(writer, "\n\t\t=>Item inserido na caixa " + label);
                FileWriter.CloseFile(writer);
            }

            StartCoroutine(DestroyObject());
        }
    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(0.5f);

        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        //Write Timer to file
        StreamWriter writer = FileWriter.OpenFile(GameInstance.HUD.studentName.text + "\\Pavilhao.txt");
        FileWriter.WriteData(writer, "\n\t\t=>Item : " + gameObject.name);
        FileWriter.WriteData(writer, "\n\t\t=>Tempo a agarrar : " + timer.ToString());
        FileWriter.CloseFile(writer);

        ItemInteractable[] itemInteractables = GameObject.FindObjectsOfType<ItemInteractable>();
        Debug.Log("Item Interactables length : " + itemInteractables.Length.ToString());
        if (itemInteractables.Length <= 0 && LevelInstance.Singleton != null)
        {
            writer = FileWriter.OpenFile(GameInstance.HUD.studentName.text + "\\Pavilhao.txt");
            FileWriter.WriteData(writer, "\nExperiencia Acabada");
            FileWriter.CloseFile(writer);

            ((SchoolLevelInstance)LevelInstance.Singleton).StopObjectFix();
        }
    }
}
