﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AircraftCameraController : MonoBehaviour
{
    public float speed = 10f;
    public Transform aircraft;
    public Transform Target;

    private void Awake()
    {
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, Target.transform.position, speed * Time.fixedDeltaTime );
        transform.LookAt(aircraft);
    }
}
