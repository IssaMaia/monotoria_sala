﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour, IPossessable
{
    protected virtual void Awake()
    {
        
    }

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {

    }

    protected virtual void FixedUpdate()
    {

    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        Actor actor = other.GetComponentInParent<Actor>();
        if (actor != null)
        {
            actor.OnBeginOverlap(this);
            OnEnterTrigger(actor);
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        Actor actor = other.GetComponentInParent<Actor>();
        if (actor != null)
        {
            actor.OnEndOverlap(this);
            OnExitTrigger(actor);
        }
    }

    protected virtual void OnBeginOverlap(Actor actor)
    {

    }

    protected virtual void OnEndOverlap(Actor actor)
    {

    }

    protected virtual void OnEnterTrigger(Actor actor)
    {

    }

    protected virtual void OnExitTrigger(Actor actor)
    {

    }

    void IPossessable.Possess(PlayerController playerController)
    {
        if (PlayerController != null)
            PlayerController.Unpossess();

        PlayerController = playerController;
        OnPossessed(playerController);
    }

    void IPossessable.Unpossess(PlayerController playerController)
    {
        if (PlayerController == null)
            return;

        OnUnpossess(playerController);
        PlayerController = null;
    }

    protected virtual void OnPossessed(PlayerController playerController)
    {

    }

    protected virtual void OnUnpossess(PlayerController playerController)
    {

    }

    public PlayerController PlayerController { get; private set; }
}
