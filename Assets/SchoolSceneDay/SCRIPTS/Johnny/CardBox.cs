﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBox : MonoBehaviour
{
    public enum ENUM_SceneType : ushort
    {
        Color = 0,
        ColorAndFormat = 1,
        Format = 2,
    }

    public ItemInteractable.ItemType itemType = ItemInteractable.ItemType.Ball;
    public ENUM_SceneType sceneType = ENUM_SceneType.Color;
    public ItemInteractable.ENUM_Color color = ItemInteractable.ENUM_Color.black;
}
