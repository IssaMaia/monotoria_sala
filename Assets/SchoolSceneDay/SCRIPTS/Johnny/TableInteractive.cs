﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableInteractive : Interactable
{
    public ChemestryInteractable2[] interactables;
    public GameObject entities;
    public GymDoorController door;

    override protected void Update()
    {
        base.Update();

        bool show = false;
        foreach (ChemestryInteractable2 c in interactables)
            show |= ChemestryInteractable2.keys.Contains(c.Key);

        interactMessage = show ? "Pousar" : "";
    }

    public void StartGame()
    {
        foreach (ChemestryInteractable2 c in interactables)
            c.StartGame();

        foreach (Transform t in entities.transform)
            t.gameObject.SetActive(true);
    }

    public override void OnHover()
    {
        foreach (ChemestryInteractable2 c in interactables)
            if (ChemestryInteractable2.keys.Contains(c.Key))
                c.Show();
    }

    public override void OnStartInteract()
    {
        foreach (ChemestryInteractable2 c in interactables)
            if (ChemestryInteractable2.keys.Contains(c.Key))
            {
                ChemestryInteractable2.keys.Remove(c.Key);
                c.Place();
            }
        bool done = true;
        foreach (ChemestryInteractable2 c in interactables)
            done &= c.gameObject.activeInHierarchy;

        if (done)
        {
            StartCoroutine(EndGame());
        }
    }

    private IEnumerator EndGame()
    {
        GameInstance.HUD.EnableTaskCompleted(true);
        yield return new WaitForSeconds(3f);
        GameInstance.HUD.EnableTaskCompleted(false);
        TeacherInteractable d = GameObject.FindObjectOfType<TeacherInteractable>();
        d.EnableGame();

        foreach (ChemestryInteractable2 c in interactables)
            c.StartGame();
    }
}
