﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInstance : MonoBehaviour
{
    protected virtual void Awake()
    {
        Singleton = this;
    }

    protected virtual void Start()
    {
    }

    protected virtual void OnDestroy()
    {
        Singleton = null;
    }

    public static LevelInstance Singleton { get; private set; }
}
