﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public enum ENUM_WaypointAction : ushort
    {
        None = 0,
        Stop = 1,
    }

    public List<Waypoint> links = new List<Waypoint>();
    public Vector2 timerMinMax = new Vector2(0f, 10f);
    public ENUM_WaypointAction action = ENUM_WaypointAction.None;
    public bool showRoute = true;
    public Color routeColor = Color.red;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = routeColor;

        if (showRoute)
            for (int i = 0; i < links.Count; i++)
                if (links[i] != null)
                    Gizmos.DrawLine(transform.position, links[i].transform.position);
    }
}