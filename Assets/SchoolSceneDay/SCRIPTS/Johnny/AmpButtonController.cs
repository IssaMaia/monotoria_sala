﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmpButtonController : MonoBehaviour
{
    public enum Side
    {
        Left = 0,
        Right = 1,
    }

    public Side side;
}
