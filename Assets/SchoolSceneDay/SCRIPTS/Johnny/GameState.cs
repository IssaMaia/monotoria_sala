﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public delegate void GameStateHandler(GameState sender);

    public event GameStateHandler OnPausedChanged;

    private bool paused;

    private void Start()
    {
    }

    public void EnableCursor(bool visible, CursorLockMode lockMode)
    {
        Cursor.lockState    = lockMode;
        Cursor.visible      = visible;
    }

    public bool Paused
    {
        get { return paused; }

        set
        {
            if (paused == value)
                return;

            paused = value;

            if (!paused)
            {
                Time.timeScale = 1f;
                EnableCursor(false, CursorLockMode.Locked);   
            }
            else
            {
                Time.timeScale = 0f;
                EnableCursor(true, CursorLockMode.None);
            }

            if (OnPausedChanged != null)
                OnPausedChanged(this);
        }
    }
}
