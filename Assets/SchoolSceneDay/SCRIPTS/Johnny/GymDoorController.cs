﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GymDoorController : MonoBehaviour
{
    public Vector3  closed, 
                    opened;

    public void ShutDoor()
    {
        transform.localRotation = Quaternion.Euler(closed);
    }

    public void OpenDoor()
    {
        transform.localRotation = Quaternion.Euler(opened);
    }
}
