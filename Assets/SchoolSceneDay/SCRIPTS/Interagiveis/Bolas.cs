﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolas : Interagiveis 
{
	Rigidbody rb;

	void Start()
	{
		rb = gameObject.AddComponent<Rigidbody> ();
	}

	public override void Interagir ()
	{
		transform.parent = Camera.main.transform;
		rb.isKinematic = true;
	}

	public override void PararDeInteragir ()
	{
		transform.parent = null;
		rb.isKinematic = false;
	}
}
