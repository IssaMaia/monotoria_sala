﻿using UnityEngine;
using System.Collections;

public class SlideLeft : MonoBehaviour
{

    // Use this for initialization
    public float doorStart;
    public float doorClosed;
    public float doorOpen;
    public float doorOpenSpeed;
    public float doorShutSpeed;
    public float doorFinalOpen;
    public int status;


    /* IEnumerator Wait()
    {
        yield return new WaitForSeconds(5f);
        transform.Translate(-0.1f, 0, 0);
        
        
    }*/

    public void Start()
    {
        doorFinalOpen = 0.0f;
        doorStart = -14.75072f;
        doorClosed = -0.0f;
        doorOpen = -23.83076f;
        doorOpenSpeed = 0.1f;
        doorShutSpeed = -0.1f;
        


        // 0 closed, 1 opened

    }
    

    // Update is called once per frame
    public void Update()
    {
        doorClosed = transform.position.x;
        doorFinalOpen = transform.position.x;

        // ################################-RC FLOOR-################################

        if (status == 1 && doorClosed > doorOpen)
        {
            transform.Translate(-0.1f, 0, 0);

        }

        if (status == 0 && doorClosed < doorStart)
        {
            transform.Translate(0.1f, 0, 0);
            

        }

       
    }
}
