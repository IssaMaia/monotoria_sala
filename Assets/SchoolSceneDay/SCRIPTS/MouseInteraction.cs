﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInteraction : MonoBehaviour {

	public LayerMask objetosInteragiveis;
	public float distanciaMaximaDeInteracao = 10;
	private Interagiveis interagivelAtual = null;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Interact ();
	}

	private void Interact()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, distanciaMaximaDeInteracao, objetosInteragiveis)) 
			{
				print (hit.collider.name);
				Interagiveis i = hit.transform.GetComponent<Interagiveis> ();
				if (i != null) 
				{
					interagivelAtual = i;
					interagivelAtual.Interagir();
				}
				if (hit.transform.GetComponent<TvButton> () != null) {
					print ("HIT BUTTON");
					hit.transform.GetComponent<TvButton> ().ProcessClick ();
				
				}
				if(hit.transform.GetComponent<loadjogopavilhao>()!= null)
					Application.LoadLevel("scene_level3");
					
			}
		}

		if (Input.GetMouseButtonUp (0)) 
		{
			if (interagivelAtual != null) 
			{
				interagivelAtual.PararDeInteragir ();
				interagivelAtual = null;
			}
		}
			
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (transform.position + transform.forward * distanciaMaximaDeInteracao, 0.1f);
	}
		
}
