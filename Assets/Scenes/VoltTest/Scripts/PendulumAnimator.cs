﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoltTest
{
    public class PendulumAnimator : MonoBehaviour
    {        
        Animator anim;
        
        void Start()
        {
            anim = GetComponent<Animator>();
        }

        public void SwingRight()
        {
            anim.SetTrigger("SwingRight");
        }

        public void SwingLeft()
        {
            anim.SetTrigger("SwingLeft");
        }
    }
}
