﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkDirection : MonoBehaviour
{
    /*
     * How it works:
     * In this script, we define which animation state we are going to call by enums.
     * Example: in anim1state, the walk state from TeacherSTM script is called and the 
     * anim1 animation is called.
     * Whenever you need to call an animation state, just call the correct enum Direction 
     * state to trigger it.
     */
    public enum Directions  ///enum with the direction animation states
    {
       default0state, //teacher in idle mode outside class
        anim1state, //teacher walking from outside to class
       default1state, //teacher in idle mode in class
       anim2state, //teacher walking to office
       default2state, //teacher sitting in her office
       accidentstate, //teacher has an accident
       anim3state //teacher walks out of the office and class
    }
    public Directions directions; //variable to manipulate enum states

    [SerializeField] private Animator animator; //get the walkdirection animator controller
    [SerializeField] private TeacherSTM teacherstm; //get the teacher state machine
    [SerializeField] private DoorSTM doorstm;

    [SerializeField] private GameObject worksound; //get the empty which plays the working sound
    [SerializeField] private GameObject fallsound; //get the empty which plays the fall sound
    [SerializeField] private GameObject chairmoving; //get the empty which plays the chair moving sound

    private void Update() {
        DirectionSTM(); //call walkdirection state machine every frame
    }

    //state machine that defines which animation will play
    private void DirectionSTM() { 
        switch(directions) {
            case Directions.default0state: //switch to teacher in idle mode outside class
                //call animation
                animator.Play("default0");
                //call idlestate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.idlestate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                break;
            case Directions.anim1state: //switch to teacher walk to enter class from outside
                //call animation
                animator.Play("anim1");
                //call walkstate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.walkstate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                break;
            case Directions.default1state: //switch to teacher in idle mode inside class
                //call animation
                animator.Play("default1");
                //call idlestate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.idlestate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                break;
            case Directions.anim2state: //switch to teacher walk to enter office 
                //call animation
                animator.Play("anim2");
                //call walkstate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.walkstate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                break;
            case Directions.default2state: //switch to teacher sitting in her office
                //call animation
                animator.Play("default2");
                //call sitstate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.sitstate;
                worksound.SetActive(true); //turn on the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                break;
            case Directions.accidentstate: //switch to teacher have an accident in her office
                //call animation
                animator.Play("accident");
                //call accidentstate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.accidentstate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(true); //turn on the empty that plays the fall sound
                break;
            case Directions.anim3state: //switch to teacher walking out of office and class
                //call animation
                animator.Play("anim3");
                //call walkstate from TeacherSTM script
                teacherstm.GetComponent<TeacherSTM>().states = TeacherSTM.States.walkstate;
                worksound.SetActive(false); //turn off the empty that plays the work sound
                fallsound.SetActive(false); //turn off the empty that plays the fall sound
                chairmoving.SetActive(true); //turn on the empty that plays the chair moving sound
                break;
        }
    }

    //method called on animation event from anim1, when called it changes the Direction state
    public void default1() {
        directions = Directions.default1state;
    }

    //method called on animation event from anim2, when called it changes the Direction state
    public void default2() {
        directions = Directions.default2state;
    }

    public void OpenDoor() {
        doorstm.doorstates = DoorSTM.DoorStates.opening;
    }

    public void CloseDoor() {
        doorstm.doorstates = DoorSTM.DoorStates.closing;
    }
    public void SemiOpenDoor() {
        doorstm.doorstates = DoorSTM.DoorStates.semiclosed;
    }
}
