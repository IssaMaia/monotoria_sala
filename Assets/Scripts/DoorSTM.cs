﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSTM : MonoBehaviour
{
    public enum DoorStates  ///enum with the door animation states
    {
        open, 
        closed,
        opening,
        closing,
        semiclosed
    }
    public DoorStates doorstates; //variable to manipulate enum states

    [SerializeField] private Animator dooranimator; //variable to get door animator
    [SerializeField] private GameObject openingdoorsound; //variable to get the sound of open door
    [SerializeField] private GameObject closingdoorsound; //variable to get the sound of closind door

    private void Start() {
        doorstates = DoorStates.closed; //it starts first in the closed state
    }

    private void Update() {
        STM(); //calling the state machine every frame
    }

    private void STM() {
        switch(doorstates) {
            case DoorStates.open: //switch to door open
                dooranimator.Play("open");
                break;
            case DoorStates.closed: //switch to door closed
                dooranimator.Play("closed");
                break;
            case DoorStates.opening: //switch to door opening
                dooranimator.Play("opening");
                openingdoorsound.SetActive(true);
                closingdoorsound.SetActive(false);
                break;
            case DoorStates.closing: //switch to door closing
                dooranimator.Play("closing");
                openingdoorsound.SetActive(false);
                closingdoorsound.SetActive(true);
                break;
            case DoorStates.semiclosed: //switch to door semiclosed
                dooranimator.Play("semiclosed");
                openingdoorsound.SetActive(false);
                closingdoorsound.SetActive(false);
                break;
        }
    }

    //methods called on animation event from opening and closing.
    //when called it changes the state of DoorStates

    public void ClosedState() {
        doorstates = DoorStates.closed;
    }
    public void OpenState() {
        doorstates = DoorStates.open;
    }
}
