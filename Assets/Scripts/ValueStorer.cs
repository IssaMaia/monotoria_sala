﻿using System;
using System.IO;
using UnityEngine;

public class ValueStorer : MonoBehaviour
{
    private static string[] paths =
        { Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        "WriteLines.txt"};

    string fullpath = Path.Combine(paths);

    public void WriteThis(string line)
    {
        line += "\n";
        System.IO.File.WriteAllText(fullpath, line);
    }
}
