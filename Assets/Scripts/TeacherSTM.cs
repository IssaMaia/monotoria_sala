﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherSTM : MonoBehaviour
{
    /*
     * How it works:
     * In this script, it defines which character model is going present in each state.
     * Here we have 4 character models: teacher in walk mode; teacher in idle mode;
     * teacher sitting and teacher in lying mode.
     * According to the state that is defined by enum, one of the models is going to be 
     * turned on and the rest turned off.
     * Example: in walk state, the teacher in walk mode is turned on and the rest turned off.
     */

    //enum with the states 
    public enum States {
        walkstate,
        idlestate, 
        sitstate,
        accidentstate
    };
   public States states; //variable to manipulate enum states

    [SerializeField] private GameObject teacher_walk; //teacher walking gameobject
    [SerializeField] private GameObject teacher_idle; //teacher idle gameobject
    [SerializeField] private GameObject teacher_sit; //teacher sit gameobject
    [SerializeField] private GameObject teacher_lying; //teacher lying down gameobject

    private void Start() {
        states = States.idlestate; //the simulation starts with setting the States to idlestate
    }

    private void Update() {
        STM(); //calling the state machine every frame
    }

    //method to switch between teacher's states
    private void STM() {
        switch(states) {
            case States.idlestate: //switch to teacher in idle mode
                teacher_walk.SetActive(false);
                teacher_idle.SetActive(true);
                teacher_sit.SetActive(false);
                teacher_lying.SetActive(false);
                break;
            case States.walkstate: //switchs to teacher walking
                teacher_walk.SetActive(true);
                teacher_idle.SetActive(false);
                teacher_sit.SetActive(false);
                teacher_lying.SetActive(false);
                break;
            case States.sitstate: //switchs to teacher sitting
                teacher_walk.SetActive(false);
                teacher_idle.SetActive(false);
                teacher_sit.SetActive(true);
                teacher_lying.SetActive(false);
                break;
            case States.accidentstate: //switchs to teacher lying down
                teacher_walk.SetActive(false);
                teacher_idle.SetActive(false);
                teacher_sit.SetActive(false);
                teacher_lying.SetActive(true);
                break;
        }
    }
}
