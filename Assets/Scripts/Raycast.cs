﻿using System.Collections;
using UnityEngine;
using EasySurvivalScripts;

/// <summary>
/// Used to fire a raycast
/// </summary>
public class Raycast : MonoBehaviour
{
    /// <summary>
    ///  Used to write text to a file;
    /// </summary>
    [SerializeField] GameObject Storer;

    /// <summary>
    /// Contains the Form to activate a desactivate
    /// </summary>
    [SerializeField] GameObject formUI;

    /// <summary>
    /// Variable used to define the state we want to activate
    /// </summary>
    [SerializeField] WalkDirection.Directions state_To_Activate;
    [SerializeField] WalkDirection.Directions state_To_Activate2;

    /// <summary>
    /// GameObject containing player
    /// </summary>
    [SerializeField] private GameObject player;

    /// <summary>
    /// GameObject containig chair position
    /// </summary>
    [SerializeField] private GameObject chairPlace;

    /// <summary>
    /// Conatains position before sitting
    /// </summary>
    [SerializeField] private GameObject beforeChair;

    /// <summary>
    /// Variable used to store the gameobject of the teacher
    /// we want to chage the state
    /// </summary>
    [SerializeField] GameObject teacher;

    /// <summary>
    /// Variables to get the player's chair audiosource and the clip which will play
    /// </summary>
    [SerializeField] private AudioSource playerchair;
    [SerializeField] private AudioClip chairclip;

    [SerializeField] private Transform lookAt;

    private float timePassed = 0.0f;
    private float timePassedSinceIsDown = 0.0f;

    private bool? used = false;

    private bool inForm = false;
    private bool teacherIsDown = false;

    private string TextToSave;

    /// <summary>
    /// Start
    /// </summary>
    private void Start()
    {
        formUI.gameObject.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    /// <summary>
    /// Update
    /// </summary>
    private void Update()
    {
        timePassed += Time.deltaTime;
        if (teacherIsDown)
        {
            timePassedSinceIsDown += Time.deltaTime;
        }
    }

    /// <summary>
    /// FixedUpdate
    /// </summary>
    void FixedUpdate()
    {
        //TEMPORARY SOLUTION
        if (used == false)
        {
            if (Input.GetKeyUp(KeyCode.E) &&
                    RayCastSender().tag == "CHAIR")
            {
                SitInChair();                
            }
        }
        else if(used == true)
        {
            if (!inForm)
            {
                if(RayCastSender().name == "LookAtOffice")
                {
                    AddToText($"Subject looked at the office" +
                        $" {timePassedSinceIsDown} after " +
                        "teacher need help. | \n");

                    inForm = false;
                    used = null;
                }
            }
            else
            {
                Camera.main.gameObject.transform.LookAt(lookAt);
            }

            if (Input.GetKeyUp(KeyCode.Escape) 
                && formUI.gameObject.activeSelf)
            {
                formUI.gameObject.SetActive(false);
                inForm = false;

                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                EasySurvivalScripts.PlayerMovement.walkSpeed = 2f;
            }
        }
        else if(used == null) //TEMPORARY SOLUTION!!!!!!
        {
            WriteText();
        }        
            
    }

    /// <summary>
    /// Method used to put player in chair
    /// </summary>
    private void SitInChair()
    {
        beforeChair.transform.position = player.transform.position;
        beforeChair.transform.rotation = player.transform.rotation;

        player.transform.position = chairPlace.transform.position;
        player.transform.rotation = chairPlace.transform.rotation;

        teacher.GetComponent<WalkDirection>().directions
        = state_To_Activate;

        //TEMPORARY SOLUTION
        used = true;

        ///fires the sit sound
        playerchair.clip = chairclip;
        playerchair.Play();

        formUI.gameObject.SetActive(true);

        AddToText($"Subject sitted {timePassed} seconds after " +
            $"beggining experience. | \n");

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        EasySurvivalScripts.PlayerMovement.walkSpeed = 0f;

        StartCoroutine(TeacherFalling());
    }

    /// <summary>
    /// Method used to send raycast
    /// </summary>
    /// <returns></returns>
    private GameObject RayCastSender()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position,
                Camera.main.transform.forward, out hit, 100.0f))
        {
            return hit.collider.gameObject;
        }

        return null;
    }

    /// <summary>
    /// Method used to add text to text to send
    /// </summary>
    /// <param name="text"></param>
    private void AddToText(string text)
    {
        TextToSave += text + "\n";
    }

    /// <summary>
    /// Writes the text in the txt file
    /// </summary>
    private void WriteText()
    {
        Storer.GetComponent<ValueStorer>().WriteThis(TextToSave);
    }

    /// <summary>
    /// Courotine used to make the teacher fall    /// 
    /// </summary>
    /// <returns></returns>
    private IEnumerator TeacherFalling()
    {
        yield return new WaitForSeconds(Random.Range(10f, 15f));

        teacher.GetComponent<WalkDirection>().directions
                = state_To_Activate2;

        AddToText($"Teacher needs assistance after" +
                $" {timePassed} seconds.  | \n");

        teacherIsDown = true;
    } 
}
