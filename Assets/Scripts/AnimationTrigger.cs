﻿using UnityEngine;

/// <summary>
/// Class used to activate animation for the teacher
/// </summary>
public class AnimationTrigger : MonoBehaviour
{
    /// <summary>
    /// Variable used to define the state we want to activate
    /// </summary>
    [SerializeField] WalkDirection.Directions state_To_Activate;

    /// <summary>
    /// Variable used to store the gameobject of the teacher
    /// we want to chage the state
    /// </summary>
    [SerializeField] GameObject teacher;

    /// <summary>
    /// Methos used to detect if an objects enters the collider
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //Detects if it was the player
        if (other.tag == "Player")
        {
            //changes the state of the teacher to the desires state
            teacher.GetComponent<WalkDirection>().directions
                = state_To_Activate;

            //TEMPORARY SOLUTION
            gameObject.SetActive(false);
        }
    }
}
